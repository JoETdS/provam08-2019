package m.joel.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //agafo informació intent
        //Intent i = getIntent();
        //String mail = i.getStringExtra("user");

        //agafo informació sharedPreferences
        sf = PreferenceManager.getDefaultSharedPreferences(this);
        String mail = sf.getString("user", "");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(mail);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                finish();
            }
        });

        Button boto = (Button) findViewById(R.id.boto_prova);
        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Botó premut", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                Button boto = (Button) findViewById(R.id.boto_prova);
                toolbar.setTitle(boto.getText());
            }
        });

        Button boto2 = (Button) findViewById(R.id.boto_prova2);
        boto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Botó premut 2", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                EditText text = (EditText) findViewById(R.id.text_prova);
                toolbar.setTitle(text.getText());
            }
        });
        /*Button boto_canvi_activity = (Button) findViewById(R.id.boto_canviActivity);
        boto_canvi_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Canvi Activity", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //iniciar nova activitat
                Intent intent = new Intent(MainActivity.this, Activity2.class);
                //intent.putExtra("taula",10);
                //intent.putExtra("cadira",15);
                EditText a = (EditText) findViewById(R.id.text_prova);
                EditText b = (EditText) findViewById(R.id.text_prova2);
                intent.putExtra("a", Integer.parseInt(a.getText().toString()));
                intent.putExtra("b", Integer.parseInt(b.getText().toString()));
                startActivity(intent);
            }
        });*/
        Button boto_canvi_activity = (Button) findViewById(R.id.boto_canviActivity);
        boto_canvi_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Canvi Activity2", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //iniciar nova activitat
                Intent intent = new Intent(MainActivity.this, Activity2.class);
                startActivityForResult(intent,1);
            }
        });

        Button boto_activity_mail = (Button) findViewById(R.id.boto_activity_mail);
        boto_activity_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Canvi ActivityMail", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //iniciar nova activitat
                Intent intent = new Intent(MainActivity.this, MailActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button boto_activity_db = (Button) findViewById(R.id.boto_activity_database);
        boto_activity_db.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Canvi ActivityDatabase", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                //iniciar nova activitat
                Intent intent = new Intent(MainActivity.this, DatabaseActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                //concatenem strings
                String str1 = data.getStringExtra("str1");
                String str2 = data.getStringExtra("str2");
                //Aquí actualizar el fragment
                TextView textView = (TextView) findViewById(R.id.text_principal);
                textView.setText(str1+str2);
            }else if(resultCode == 10){
                int a = data.getIntExtra("a",0);
                int b = data.getIntExtra("b",0);
                int c = a+b;
                TextView textView = (TextView) findViewById(R.id.text_principal);
                textView.setText(String.valueOf(c));
            }else if(resultCode == 20){
                int a = data.getIntExtra("a",0);
                int b = data.getIntExtra("b",0);
                int c = a*b;
                TextView textView = (TextView) findViewById(R.id.text_principal);
                textView.setText(String.valueOf(c));
            }else if(resultCode == 30){
                int a = data.getIntExtra("a",0);
                int b = data.getIntExtra("b",0);
                int c = (int) Math.pow(a, b);
                TextView textView = (TextView) findViewById(R.id.text_principal);
                textView.setText(String.valueOf(c));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
