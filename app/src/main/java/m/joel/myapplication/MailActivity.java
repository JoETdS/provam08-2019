package m.joel.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MailActivity extends AppCompatActivity {

    SharedPreferences sf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Adéu!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                sf =PreferenceManager.getDefaultSharedPreferences(MailActivity.this);
                SharedPreferences.Editor editor = sf.edit();
                editor.putBoolean("registered", false);
                editor.apply();
                finish();
            }
        });

        Button buttonMail = (Button) findViewById(R.id.button_send_mail);
        buttonMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                sendMail();
            }
        });
        Button buttonSMS = (Button) findViewById(R.id.button_send_sms);
        buttonSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                sendSMS();
            }
        });
        Button buttonCall = (Button) findViewById(R.id.button_phone_number);
        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                callNumber();
            }
        });
    }

    private void sendMail() {
        EditText etMail = findViewById(R.id.text_mail);
        EditText etSubject = findViewById(R.id.subject);
        EditText etBody = findViewById(R.id.text_body);

        String mail = etMail.getText().toString();
        String subject = etSubject.getText().toString();
        String body = etBody.getText().toString();
        String[] mails = mail.split(",");
        Intent i = new Intent(Intent.ACTION_SEND);

        i.setType("text/plain");
        //i.putExtra(Intent.EXTRA_EMAIL, new String[] { mail });
        i.putExtra(Intent.EXTRA_EMAIL, mails);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(i, getString(R.string.message_mail)));


    }

    private void sendSMS(){
        EditText etNumber = findViewById(R.id.text_mail);
        EditText etBody = findViewById(R.id.text_body);

        String number = etNumber.getText().toString();
        String body = etBody.getText().toString();
        Intent i = new Intent(Intent.ACTION_VIEW);
        //Intent i = new Intent(Intent.ACTION_SEND);
        // Invokes only SMS/MMS clients
        //i.setType("vnd.android-dir/mms-sms");
        // Specify the Phone Number
        i.setData(Uri.parse("smsto:"));
        i.putExtra("address", number);
        // Specify the Message
        i.putExtra("sms_body", body);
        startActivity(i);
    }

    private void callNumber(){
        Intent callIntent = new Intent(Intent.ACTION_DIAL); //obrir l'eina de trucada
        EditText etNumber = findViewById(R.id.text_mail);
        String number = etNumber.getText().toString();

        callIntent.setData(Uri.parse("tel:"+number));
        startActivity(callIntent);
    }

}
