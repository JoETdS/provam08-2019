package m.joel.myapplication;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DatabaseActivity extends AppCompatActivity {

    private SQLiteDatabase mydatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //SI LA BD NO EXISTEIX LA CREEM
            this.mydatabase = this.openOrCreateDatabase("activitat", MODE_PRIVATE, null);
        //SI LA TAULA NO EXISTEIX LA CREEM
            String sqlCreate = "CREATE TABLE IF NOT EXISTS activitat " +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " nom TEXT) ";
            this.mydatabase.execSQL(sqlCreate);

        Button desar = (Button) findViewById(R.id.boto_desar);
        desar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                desarDades();
                Snackbar.make(view, "Dades desades", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Button carregar = (Button) findViewById(R.id.boto_carregar);
        carregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carregarDades();
                Snackbar.make(view, "Dades carregades", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    private void desarDades() {
        TextView tvNom = (TextView) findViewById(R.id.text_desar);
        String nom = tvNom.getText().toString();
        String sqlQuery = "INSERT INTO activitat (nom) VALUES ('"+nom+"');";
        this.mydatabase.execSQL(sqlQuery);
    }

    private void carregarDades() {
        String sqlQuery = "Select * from activitat";
        Cursor resultSet = this.mydatabase.rawQuery(sqlQuery,null);
        String dades = "";
        if(comprovarTextBox()){
            resultSet.moveToFirst();
            String id = resultSet.getString(0);
            String name = resultSet.getString(1);
            dades = id + " - "+name;
        }else{
            EditText etPos = (EditText)findViewById(R.id.text_pos);
            String str = etPos.getText().toString();
            int pos = Integer.parseInt(str);
            resultSet.moveToPosition(pos-1);
            String id = resultSet.getString(0);
            String name = resultSet.getString(1);
            dades = id + " - "+name;
        }
        TextView tv = findViewById(R.id.text_carregat);
        tv.setText(dades);
    }

    private boolean comprovarTextBox() {
        EditText etPos = (EditText)findViewById(R.id.text_pos);
        String str = etPos.getText().toString();
        if(str.equals("")){
            return true;
        }
        return false;
    }

}
