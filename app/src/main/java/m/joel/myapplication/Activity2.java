package m.joel.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //agafo l'Intent que he passat des de l'altra activitat
        //Intent intent = getIntent();
        /*int taula = intent.getIntExtra("taula",0);
        int cadira = intent.getIntExtra("cadira",0);
        String text = intent.getStringExtra("text_prova");
        */
        /*int a = intent.getIntExtra("a",0);
        int b = intent.getIntExtra("b",0);
        int c = a+b;
        //creo instància del textView que tinc a content_2.xml
        TextView textView = (TextView) findViewById(R.id.text_activity_2);
        textView.setText("El resultat és: " + c);
*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        /*Per passar les strings
        Button boto = (Button) findViewById(R.id.multiplicar);
        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent i = new Intent(Activity2.this, MainActivity.class);
                //i.putExtra("producte", producte());

                //ara passem strings i les concatenem
                EditText valorA = (EditText) findViewById(R.id.valorA);
                EditText valorB = (EditText) findViewById(R.id.valorB);
                i.putExtra("str1",valorA.getText().toString());
                i.putExtra("str2",valorB.getText().toString());
                setResult(RESULT_OK, i);
                finish();
            }
        });*/
        //sumem els valors
        Button sumar = (Button) findViewById(R.id.sumar);
        sumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent i = new Intent(Activity2.this, MainActivity.class);
                //i.putExtra("producte", producte());

                //ara passem strings i les concatenem
                EditText valorA = (EditText) findViewById(R.id.valorA);
                EditText valorB = (EditText) findViewById(R.id.valorB);
                int a =Integer.parseInt(valorA.getText().toString());
                int b =Integer.parseInt(valorB.getText().toString());
                i.putExtra("a",a);
                i.putExtra("b",b);
                setResult(10, i);
                finish();
            }
        });
        //multipliquem els valors
        Button multiplicar = (Button) findViewById(R.id.multiplicar);
        multiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent i = new Intent(Activity2.this, MainActivity.class);
                //i.putExtra("producte", producte());

                //ara passem strings i les concatenem
                EditText valorA = (EditText) findViewById(R.id.valorA);
                EditText valorB = (EditText) findViewById(R.id.valorB);
                int a =Integer.parseInt(valorA.getText().toString());
                int b =Integer.parseInt(valorB.getText().toString());
                i.putExtra("a",a);
                i.putExtra("b",b);
                setResult(20, i);
                finish();
            }
        });
        //potencia dels valors
        Button potencia = (Button) findViewById(R.id.potencia);
        potencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent i = new Intent(Activity2.this, MainActivity.class);
                //i.putExtra("producte", producte());

                //ara passem strings i les concatenem
                EditText valorA = (EditText) findViewById(R.id.valorA);
                EditText valorB = (EditText) findViewById(R.id.valorB);
                int a =Integer.parseInt(valorA.getText().toString());
                int b =Integer.parseInt(valorB.getText().toString());
                i.putExtra("a",a);
                i.putExtra("b",b);
                setResult(30, i);
                finish();
            }
        });
    }
    private int producte(){
        EditText valorA = (EditText) findViewById(R.id.valorA);
        EditText valorB = (EditText) findViewById(R.id.valorB);

        int a = Integer.parseInt(valorA.getText().toString());
        int b = Integer.parseInt(valorB.getText().toString());

        return a*b;
    }

}
